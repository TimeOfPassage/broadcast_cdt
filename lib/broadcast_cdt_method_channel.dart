import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'broadcast_cdt_platform_interface.dart';

/// An implementation of [BroadcastCdtPlatform] that uses method channels.
class MethodChannelBroadcastCdt extends BroadcastCdtPlatform {
  /// Flutter send message to Android or iOS
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('broadcast_cdt_method_channel');

  /// Android or iOS send message to Flutter
  final eventChannel = const EventChannel('broadcast_cdt_event_channel');

  @override
  Future<String?> getPlatformVersion() async {
    return await methodChannel.invokeMethod<String>('getPlatformVersion');
  }

  @override
  Future<EventChannel?> startLocationReceiver() async {
    bool isStartSuccess = await methodChannel.invokeMethod<bool>('startLocationReceiver') ?? false;
    if (isStartSuccess) {
      return eventChannel;
    }
    return null;
  }

  @override
  Future<bool?> removeLocationReceiver() async {
    return await methodChannel.invokeMethod<bool>('removeLocationReceiver');
  }
}
