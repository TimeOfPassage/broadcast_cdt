import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:broadcast_cdt/broadcast_cdt.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  bool isNotWatcher = false;
  Map battery = {};
  final _broadcastCdtPlugin = BroadcastCdt();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = await _broadcastCdtPlugin.getPlatformVersion() ??
          'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Text("PlatformVersion is $_platformVersion"),
              !isNotWatcher
                  ? ElevatedButton(
                      onPressed: () {
                        _broadcastCdtPlugin
                            .startLocationReceiver()
                            .then((eventChannel) {
                          if (eventChannel == null) {
                            return;
                          }
                          setState(() {
                            isNotWatcher = true;
                          });
                          eventChannel.receiveBroadcastStream().listen((event) {
                            setState(() {
                              print(event);
                              // battery = jsonDecode(event);
                            });
                          });
                        });
                      },
                      child: const Text("start"),
                    )
                  : const SizedBox.shrink(),
              isNotWatcher
                  ? ElevatedButton(
                      onPressed: () {
                        _broadcastCdtPlugin
                            .removeLocationReceiver()
                            .then((value) {
                          setState(() {
                            isNotWatcher = false;
                          });
                        });
                      },
                      child: const Text("stop"),
                    )
                  : const SizedBox.shrink(),
            ]),
      ),
    );
  }
}
